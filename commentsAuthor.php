<?php
// Connect to the database 
include_once("connection.php");

// Store in an array the (distinct) authors stored in the collection
/**********
** YOUR CODE HERE:
Assign to $authorArray all the distinct authors of comments.
See the 'distinct' method of MongoDB collections
**********/

echo '<h4>Autores de comentarios</h4>';
echo '<ol class="list-unstyled">';

// Show the authors
// Each author is an hyperlink to index.php with this two GET parameters:
// command: 'showPostsCommentedByAuthor' 
// name: the name of the author

/**********
** YOUR CODE HERE:
Iterate through the array of authors (use the variable $authorArray)
For each author, print an hyperlink with the full name of the author
The link points to index.php with two parameters:
* command =  showPostsCommentedByAuthor
* author = the full name of the author
**********/
$authorArray = [];
//$documents = $collection->find([],['limit' => 1],['sort' => ['comments.author' => -1]]);
$documents = $collection->find([],['limit' => 1]);
foreach ($documents as $document) {
	$comments = $document['comments'];
	foreach ($comments as $comment) {
		//echo $comment['author'];
		if(!in_array($comment['author'],$authorArray)){
			array_push($authorArray, $comment['author']);
		}
	}
}

//asort($authorArray);
foreach ($authorArray as $author) {
	if($author!=""){
		$link = str_replace(" ", "%", $author);
		echo '<a href = index.php?command=showPostsCommentedByAuthor&author=' . $link . '>' . $author . '</a>';
		if($author!=$authorArray[count($authorArray)-1]){
			echo "<br>";
		}
	}

	
}

echo '</ol>';
?>
