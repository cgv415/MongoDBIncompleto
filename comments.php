<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $document has been initialized with the current post

echo '<h2>Comentarios</h2>';

// Show the comments of the current post
echo '<ul>';

/**********
** YOUR CODE HERE:
Iterate through the post (use the variable $document) and get the comments
For each comment print only the first 100 characters (use the PHP substr() function)
**********/
$comments = $document['comments'];

$count = count($comments);
//var_dump($comments[0]['body']);
for($i = 0; $i < 5;$i++){
	if($i != (0)){
		echo "<br>";	
	}
	$comment = substr($comments[$i]['body'], 0, 100) . '...';
	echo "Author: ". $comments[$i]['author']. "<br>" .$comment;	
	
	if($i != 5){
		echo "<br>";	
	}
}

//echo "Count: " . $count;
echo '</ul>';
?>
